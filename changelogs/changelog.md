
## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!1

---

## 0.1.1 [05-02-2022]

- Initial Commit

See commit 00871e9

---
