
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:07PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_devicemanager!12

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-adtran_mosaic_devicemanager!10

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:15PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_devicemanager!9

---

## 0.4.2 [08-06-2024]

* Changes made at 2024.08.06_19:28PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_devicemanager!8

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_14:47PM

See merge request itentialopensource/adapters/adapter-adtran_mosaic_devicemanager!7

---

## 0.4.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!6

---

## 0.3.3 [03-28-2024]

* Changes made at 2024.03.28_13:37PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:56PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:23AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!3

---

## 0.3.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-adtran_mosaic_devicemanager!1

---

## 0.1.1 [05-02-2022]

- Initial Commit

See commit 00871e9

---
