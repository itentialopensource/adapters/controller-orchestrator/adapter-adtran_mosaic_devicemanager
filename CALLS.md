## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Mosaic Device Manager. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Mosaic Device Manager.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adtran Mosaic Device Manager. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">updateListOfDevicesConnectedToACS(body, callback)</td>
    <td style="padding:15px">Add a new device to the list of devices connected to the ACS</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOfDeviceTypesDefindedInACS(first, count, callback)</td>
    <td style="padding:15px">Retrieves the list of device types defined in the ACS</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceType(body, callback)</td>
    <td style="padding:15px">Add a new device type</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOfDeviceTypesCheckingInToACS(deviceTypeId, callback)</td>
    <td style="padding:15px">Retrieves the list of device types defined in the ACS</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDeviceType(deviceTypeId, body, callback)</td>
    <td style="padding:15px">Updates a single device type</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceType(deviceTypeId, callback)</td>
    <td style="padding:15px">Remove Device Type</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceStatisticsByManufacturer(callback)</td>
    <td style="padding:15px">Retrieves the counts of different devices by manufacturer</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/manufacturers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countsOfDeviceClassesByManufacturer(manufacturer, callback)</td>
    <td style="padding:15px">Retrieves the counts of different device product classes by manufacturer</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/manufacturers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dailyDeviceCountsFor7Days(callback)</td>
    <td style="padding:15px">Retrieves the number of new devices seen each day, over the last 7 days</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/recentActivity/newDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOfDevicesCheckedInFor7Days(callback)</td>
    <td style="padding:15px">Retrieves the number of devices that have checked in each day, over the last 7 days</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/recentActivity/devicesInforming?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOfDevicesByDeviceType(first, count, days, callback)</td>
    <td style="padding:15px">Retrieves the number of devices by device type, ordered descending by count</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/deviceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOfSubscribersByLabel(first, count, callback)</td>
    <td style="padding:15px">Retrieves the number of subscribers by label, ordered descending by count</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/labels/subscriberCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceSolicitStatus(deviceIds, callback)</td>
    <td style="padding:15px">Gets the solicit status</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/solicits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceStatus(deviceId, body, callback)</td>
    <td style="padding:15px">Queues devices to update their status</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/stats/solicits/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firmwareResourceListByDeviceType(deviceTypeId, first, count, deviceType, callback)</td>
    <td style="padding:15px">List the firmware</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFirmwareResourceByDeviceType(deviceTypeId, body, callback)</td>
    <td style="padding:15px">Add new firmware to all devices of the specified type</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firmwareDescription(deviceTypeId, firmwareId, callback)</td>
    <td style="padding:15px">Retrieves a single firmware description</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeFirmwareDescription(deviceTypeId, firmwareId, callback)</td>
    <td style="padding:15px">Removes a single firmware description</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyFirmwareDescription(deviceTypeId, firmwareId, body, callback)</td>
    <td style="padding:15px">Updates a single firmware description</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">firmwareResourceLabelsByIDForDeviceType(deviceTypeId, firmwareId, callback)</td>
    <td style="padding:15px">Retrieves the labels associated to the firmware resource</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyFirmwareLabels(deviceTypeId, firmwareId, body, callback)</td>
    <td style="padding:15px">Updates the labels for a firmware</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/types/{pathv1}/firmware/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countOfDevicesInACSByDeviceType(callback)</td>
    <td style="padding:15px">Retrieves a count of the devices are in the system, broken out by device type</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDetailsGET(id, callback)</td>
    <td style="padding:15px">Gets basic device information for a device including its identification, inform information and dev</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplate(callback)</td>
    <td style="padding:15px">Get empty JSON template to populate for creating a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/api/v1/templates/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevicePOST(body, callback)</td>
    <td style="padding:15px">Creates a new future device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/api/v1/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(id, callback)</td>
    <td style="padding:15px">Delete the device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/api/v1/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockDevice(id, callback)</td>
    <td style="padding:15px">Lock a device for web service writes</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/lockDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceAttributeList(id, recursive, withValues, withLeaves, attributeDate, filter, callback)</td>
    <td style="padding:15px">Gets a list of device attributes</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/attributes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDeviceAttributes(id, reboot, body, callback)</td>
    <td style="padding:15px">Creates an attribute update action which sets the attributes to the specified values and adds it to</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/attributes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceAttributes(id, reboot, body, callback)</td>
    <td style="padding:15px">Creates an attribute create action which creates new objects under the 'parent' attribute and adds</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/attributes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceAttributes(id, callback)</td>
    <td style="padding:15px">Creates an attribute delete action which deletes the given objects and adds it to the device's pend</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/attributes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestDeviceAttributeRefresh(id, body, callback)</td>
    <td style="padding:15px">Creates an action which will cause the device to send values for the requested attributes</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/attributes/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceNewActionList(id, since, callback)</td>
    <td style="padding:15px">Retrieves a list of recent actions added to this device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queuedActionDetail(id, scriptRunId, callback)</td>
    <td style="padding:15px">Retrieves the detail for a single action which has been added to a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/actions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyScriptIDAndParametersForQueuedAction(id, scriptRunId, body, callback)</td>
    <td style="padding:15px">Updates the script ID and parameters for a single queued action</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/actions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queuedAction(id, scriptRunId, callback)</td>
    <td style="padding:15px">Removes a single queued action. Only scripts created by the current user and are "SCHEDULED" can be</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/actions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">triggerDeviceEvent(id, body, callback)</td>
    <td style="padding:15px">Fires an event for the related device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceLabels(id, callback)</td>
    <td style="padding:15px">Gets the labels for a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyDeviceLabels(id, body, callback)</td>
    <td style="padding:15px">Sets all labels for a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceLabels(id, body, callback)</td>
    <td style="padding:15px">Adds the given labels for a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceStatisticsSummary(id, callback)</td>
    <td style="padding:15px">Returns a summary of miscellaneous statistics about a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">solicitStatus(id, callback)</td>
    <td style="padding:15px">Gets the solicit status</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceUpdateQueue(id, body, callback)</td>
    <td style="padding:15px">Queues devices to update their status</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/solicit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceSessionTraceList(id, callback)</td>
    <td style="padding:15px">Returns a summary of recent trace logs for the device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceTraceLog(id, body, callback)</td>
    <td style="padding:15px">Begin a new trace log for the device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceTraceLevel(id, callback)</td>
    <td style="padding:15px">Gets the logging level for the device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureDeviceLoggingLevel(id, body, callback)</td>
    <td style="padding:15px">Configure logging level for the device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceSessionTraceSummary(id, traceId, callback)</td>
    <td style="padding:15px">Returns a summary of the session trace log for a device</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceSessionTraceDetail(id, traceId, callback)</td>
    <td style="padding:15px">Retrieve the details of a single CPE session</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/devices/{pathv1}/trace/level{pathv2}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accessDevicesOnActivationServer(first, count, q, callback)</td>
    <td style="padding:15px">Gets devices known by the activation server</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/activation/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aCSSubscriberList(callback)</td>
    <td style="padding:15px">Retrieves a list of subscribers in the system</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewACSSubscriberPOST(body, callback)</td>
    <td style="padding:15px">Creates a new subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/api/v2/subscribers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestPasswordChange(body, callback)</td>
    <td style="padding:15px">Requests an automated change of password with email notification</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/resetPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statisticsForSubscribers(callback)</td>
    <td style="padding:15px">Gets summary information for all active subscribers</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statisticsForSpecificSubscriber(personId, callback)</td>
    <td style="padding:15px">Retrieve a subscriber indicated by {personId}.</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySubscriber(personId, body, callback)</td>
    <td style="padding:15px">Update a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriber(personId, callback)</td>
    <td style="padding:15px">Delete an existing subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberLabelSet(personId, callback)</td>
    <td style="padding:15px">Retrieves the subscriber's set of labels</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">defineLabelsForSubscriber(personId, body, callback)</td>
    <td style="padding:15px">Sets the labels for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubscriberLabels(personId, body, callback)</td>
    <td style="padding:15px">Adds the labels for a subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberManagementGroups(personId, cA, q, callback)</td>
    <td style="padding:15px">Retrieves the list of management groups associated to the subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubscriberManagementGroup(personId, body, callback)</td>
    <td style="padding:15px">Creates a management group for the subscriber.</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberManagementGroupDetails(personId, managementGroupId, callback)</td>
    <td style="padding:15px">Returns a management group for the subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySubscriberManagementGroup(personId, managementGroupId, body, callback)</td>
    <td style="padding:15px">Updates a management group for the subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberManagementGroup(personId, managementGroupId, callback)</td>
    <td style="padding:15px">Deletes this management group</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriptionsForManagementGroup(personId, managementGroupId, callback)</td>
    <td style="padding:15px">Lists the subscriptions in the current management group</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubscriptionToManagementGroup(personId, managementGroupId, body, callback)</td>
    <td style="padding:15px">Adds a subscription to the management group</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceSubscriptionDetails(personId, managementGroupId, packageName, callback)</td>
    <td style="padding:15px">Retrieve information about a subscription. to a service with the specified {packageName}</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/subscription/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriptionPackageStatus(personId, managementGroupId, packageName, body, callback)</td>
    <td style="padding:15px">Updates the status of the given subscription package</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/subscription/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriptionPackage(personId, managementGroupId, packageName, callback)</td>
    <td style="padding:15px">Deletes the specified subscription</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/subscription/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managementGroupDeviceSignature(personId, managementGroupId, callback)</td>
    <td style="padding:15px">Returns the device signature of the management group</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/deviceSignature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managementGroup(personId, managementGroupId, body, callback)</td>
    <td style="padding:15px">Updates the properties of this management group</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/labels/managementGroups/{pathv2}/deviceSignature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberAttributeTree(personId, callback)</td>
    <td style="padding:15px">Retrieves the subscriber's attribute tree</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberAttribute(personId, body, callback)</td>
    <td style="padding:15px">Updates the attributes for the given subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberAttributes(personId, body, callback)</td>
    <td style="padding:15px">Creates the attributes for the given subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriberAttributes(personId, name, callback)</td>
    <td style="padding:15px">Removes a portion of the attribute tree for the given subscriber</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(personId, body, callback)</td>
    <td style="padding:15px">Self-administered subscriber password within the control panel</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/subscribers/{pathv1}/changePassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubscriberLoginSessions(body, callback)</td>
    <td style="padding:15px">Creates Subscriber Login Session</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/control-panel/session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">subscriberLoginSession(tokenId, body, callback)</td>
    <td style="padding:15px">Updates a subscriber's login session</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/control-panel/session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriberLoginSession(tokenId, callback)</td>
    <td style="padding:15px">Removes the session</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/control-panel/session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionExpirationStatus(callback)</td>
    <td style="padding:15px">Gets the session expiration status</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/control-panel/session/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aCSUserListGET(callback)</td>
    <td style="padding:15px">Lists the set of ACS users</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyACSUserList(body, callback)</td>
    <td style="padding:15px">Add a user to the ACS</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userDetails(login, callback)</td>
    <td style="padding:15px">Gets user details for the given login name</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyUserDetails(login, body, callback)</td>
    <td style="padding:15px">Change the user's details</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(login, callback)</td>
    <td style="padding:15px">Removes a user</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userNotificationsForPast10Minutes(login, callback)</td>
    <td style="padding:15px">Gets the current set of notifications occurred in the last 10 minutes</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserNotification(login, body, callback)</td>
    <td style="padding:15px">Adds a user notification</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyUserPreferences(login, body, callback)</td>
    <td style="padding:15px">Updates the user's preferences</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/preferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserRoles(login, callback)</td>
    <td style="padding:15px">Lists the user's set of roles</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserRoles(login, body, callback)</td>
    <td style="padding:15px">Updates user's roles</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserLabels(login, callback)</td>
    <td style="padding:15px">Gets the labels for a user</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserLabels(login, body, callback)</td>
    <td style="padding:15px">Updates the labels for a user</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/users/{pathv1}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetaDataForSubscribers(filter, callback)</td>
    <td style="padding:15px">Gets a tree representing the subscriber attribute schema (metadata)</td>
    <td style="padding:15px">{base_path}/{version}/prime-home/portal/schema/device/subscriber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
