# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the AdtranMosaicDeviceManager System. The API that was used to build the adapter for AdtranMosaicDeviceManager is usually available in the report directory of this adapter. The adapter utilizes the AdtranMosaicDeviceManager API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Mosaic Device Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Mosaic Device Manager. With this adapter you have the ability to perform operations with Mosaic Device Manager on items such as:

- Device Management
- Subscriber Management

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
