# Adtran Mosaic Device Manager

Vendor: Adtran
Homepage: https://www.adtran.com/en

Product: Mosaic Device Manager
Product Page: https://www.adtran.com/en/products-and-services/residential-solutions/cloud-managed-wi-fi/mosaic-device-manager

## Introduction
We classify Mosaic Device Manager into the Data Center and Network Services domaina as Mosaic Device Manager provides a Controller/Orchestrator solution.

"Mosaic Device Manager, a powerful tool designed for service providers navigating a complex landscape of diverse vendors and technologies."
"Mosaic Device Manager equips CSRs with AI-powered tools and comprehensive analytics on subscriber, device and application usage trends, enabling effective issue resolution and proactive service enhancements."

## Why Integrate
The Mosaic Device Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Mosaic Device Manager. With this adapter you have the ability to perform operations with Mosaic Device Manager on items such as:

- Device Management
- Subscriber Management

## Additional Product Documentation
The [Mosaic Device Manager API Overview](https://kb.smartrg.com/DM_KnowledgeBase/Content/User_Manual/BP_API_Integration_Points.htm?TocPath=_____10)